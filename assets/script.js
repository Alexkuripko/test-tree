$(document).ready(function () {
    $('.create').on('click', function () {
        let keys = this.id, name = $(this).siblings('input').val();
        $.ajax({
            url: '/create',
            type: 'post',
            data: {
                create: keys,
                name: name
            },
            success: function (data) {
                $('body').html(data);
            }
        });
    });

    $('.update').on('click', function () {
        let id = this.id, name = $(this).siblings('input').val();
        $.ajax({
            url: '/update',
            type: 'post',
            data: {
                id: id,
                name: name
            },
            success: function (data) {
                $('body').html(data);
            }
        });
    });

    $('.delete').on('click', function () {
        let keys = this.id;
        $.ajax({
            url: '/delete',
            type: 'post',
            data: {
                delete: keys
            },
            success: function (data) {
                $('body').html(data);
            }
        });
    });
    let interval;
    $('#delModal').on('click', function () {
        let time = 20;
        $('.timer').text(time);
        interval = setInterval(function () {
            $('.timer').text(time);
            time--;
            if (time < 0) {
                $('#deleteModal').modal('hide');
                clearInterval(interval);
            }
        }, 1000)
    });
    $('.clear').on('click', function () {
        clearInterval(interval);
    });
});
