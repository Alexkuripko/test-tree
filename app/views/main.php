<!DOCTYPE html>
<html>
<head>
    <title>TREE</title>
    <link rel="stylesheet" href="../../assets/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../assets/styles.css">
</head>
<body>
<div id="content" class="container">

    <?php if (empty($tree)): ?>
        <button type="button" class="btn btn-success btn-create" data-toggle="modal" data-target="#createModal">
            Create root
        </button>
    <?php endif; ?>

    <?php /** @var array $tree */
    foreach ($tree as $node): ?>
        <div class="node" id="<?= $node['right_key'] ?>" style=" margin-left: <?= $node['level'] * 40 ?>px">
            <i class="fa fa-arrow-right" aria-hidden="true"></i>
            <span class="node-name"><?= $node['name'] ?></span>
            <i class="fa fa-pencil" aria-hidden="true" data-toggle="modal"
               data-target="#updateModal<?= $node['id'] ?>"></i>
            <i class="fa fa-plus-square-o" data-toggle="modal" data-target="#createModal<?= $node['id'] ?>"
               aria-hidden="true"> </i>
            <i class="fa fa-minus-square-o
                <?php if ($node['left_key'] == 1): ?>
                      " id="delModal" data-toggle="modal" data-target="#deleteModal"
                <?php else: ?>
               delete" id="<?= $node['left_key'] . '/' . $node['right_key'] ?>"
            <?php endif; ?>
            aria-hidden="true"> </i>
        </div>
        <div class="modal fade clear" id="updateModal<?= $node['id'] ?>" tabindex="-1" role="dialog"
             aria-labelledby="updateModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="updateModalLabel">Edit node name</h5>
                        <button type="button" class="close clear" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Enter new node name
                    </div>
                    <div class="modal-footer">
                        <input class="form-control" value="<?= $node['name'] ?>" type="text" maxlength="10" required>
                        <button type="button" class="btn btn-primary update" id="<?= $node['id'] ?>"
                                data-dismiss="modal">Update
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="createModal<?= $node['id'] ?>" tabindex="-1" role="dialog"
             aria-labelledby="createModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="createModalLabel">
                            Creating a new root</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Enter root name
                    </div>
                    <div class="modal-footer">
                        <input class="form-control root-name" value="node" type="text" maxlength="10" required>
                        <button type="button" class="btn btn-primary create"
                                id="<?= $node['right_key'] . '/' . $node['level'] ?>" data-dismiss="modal">Add node
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<div class="modal fade clear" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Delete confirmation</h5>
                <button type="button" class="close clear" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                This is very dangerous, you shouldn't do it! Are you really really shure?
            </div>
            <div class="modal-footer">
                <span class="timer">20</span>
                <button type="button" class="btn btn-primary delete" id="1/10000" data-dismiss="modal">Yes I am</button>
                <button type="button" class="btn btn-outline-secondary clear" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createModalLabel">
                    Creating a new root</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Enter root name
            </div>
            <div class="modal-footer">
                <input class="form-control root-name" value="root" type="text" maxlength="10" required>
                <button type="button" class="btn btn-primary create"
                        id="1/0" data-dismiss="modal">Add root
                </button>
            </div>
        </div>
    </div>
</div>
<script src="/assets/jquery.js"></script>
<script src="/assets/script.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>

