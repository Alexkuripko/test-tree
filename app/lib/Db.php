<?php

namespace app\lib;

use PDO;

class Db
{
    protected $db;

    /**
     * @return PDO
     */
    public static function getConnection()
    {

        $config = require 'app/config/db.php';
        $db = new PDO('mysql:host=' . $config['host'] . ';dbname=' . $config['name'] . ';charset=utf8', $config['user'], $config['password']);
        if (!$db) {
            die("Connection failed: " . mysqli_connect_error());
        }
        return $db;
    }

}