<?php
return [
    'create' => 'main/create',
    'update' => 'main/update',
    'delete' => 'main/delete',
    '' => 'main/index'
];