<?php

namespace app\models;

use app\lib\Db;
use PDO;

class QueryDb
{
    protected $connection;

    public function __construct()
    {
        $this->connection = Db::getConnection();
    }

    /**
     * @param array $data
     */
    public function create($data)
    {
        $name = $data['name'];
        $left_key = $data['left_key'];
        $right_key = $data['right_key'];
        $level = $data['level'];

        $this->connection->prepare("UPDATE nodes SET right_key = right_key + 2, left_key = IF(left_key > $left_key ,
        left_key + 2, left_key) 
        WHERE right_key >= $left_key")->execute();

        $this->connection->prepare("INSERT INTO nodes (name, left_key, right_key, level)
                VALUES('$name', '$left_key', '$right_key','$level')")->execute();
    }

    /**
     * @return array
     */
    public function select()
    {
        $result = $this->connection->prepare("SELECT * FROM nodes ORDER BY left_key");
        $result->execute();
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }


    /**
     * @param $id
     * @param $name
     */
    public function update($id, $name)
    {
        $this->connection->prepare("UPDATE nodes SET name = '$name' WHERE id = $id")->execute();
    }

    /**
     * @param $data
     */
    public function delete($data)
    {
        $left_key = $data[0];
        $right_key = $data[1];
        $diff = $right_key - $left_key + 1;
        $this->connection->prepare("DELETE FROM nodes 
        WHERE left_key >= $left_key  AND right_key <= $right_key")->execute();
        $this->connection->prepare("UPDATE nodes SET left_key = left_key - ,
         right_key = right_key -  $diff  WHERE left_key > $right_key")->execute();
        $this->connection->prepare("UPDATE nodes SET 
        right_key = right_key -  $diff  WHERE left_key = 1")->execute();
    }

}

