<?php

namespace app\models;

use app\models\QueryDb;

class Tree extends QueryDb
{

    public function createNode()
    {
        $keys = explode('/', $_POST['create']);
        $level = $keys[1] + 1;
        $newNode['name'] = $_POST['name'];
        $newNode['left_key'] = $keys[0];
        $newNode['right_key'] = $keys[0] + 1;
        $newNode['level'] = $level;
        $this->create($newNode);
    }

    public function updateNode()
    {
        $id = $_POST['id'];
        $name = $_POST['name'];
        $this->update($id, $name);
    }

    public function deleteNode()
    {
        $keys = explode('/', $_POST['delete']);
        $this->delete($keys);
    }

    /**
     * @return array
     */
    public function getTree()
    {
        return $this->select();
    }
}