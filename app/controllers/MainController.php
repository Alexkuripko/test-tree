<?php

use app\models\Tree;

class MainController
{
    public $model;

    public function __construct()
    {
        $this->model = new Tree();
    }

    public function actionIndex()
    {
        $tree = $this->model->getTree();
        include_once './app/views/main.php';
    }

    public function actionCreate()
    {
        $this->model->createNode();
        $this->actionIndex();
    }

    public function actionUpdate()
    {
        $this->model->updateNode();
        $this->actionIndex();
    }


    public function actionDelete()
    {
        $this->model->deleteNode();
        $this->actionIndex();
    }
}